package net.downwithdestruction.dwdfly.listener;

import net.downwithdestruction.dwdfly.Logger;
import net.downwithdestruction.dwdfly.Main;
import net.downwithdestruction.dwdfly.task.NoFallDamage;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
    private final Main plugin;

    public PlayerListener(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        player.setAllowFlight(false);
        player.setFlying(false);
        player.setFlySpeed(0.1F);

        Logger.debug("Reset fly mode (off) and speed (1) for " + player.getName());

        new NoFallDamage(player).runTaskTimer(plugin, 5, 5);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();

        player.setAllowFlight(false);
        player.setFlying(false);
        player.setFlySpeed(0.1F);

        Logger.debug("Reset fly mode (off) and speed (1) for " + player.getName());

        new NoFallDamage(player).runTaskTimer(plugin, 5, 5);
    }
}
