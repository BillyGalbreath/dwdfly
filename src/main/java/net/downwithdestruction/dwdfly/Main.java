package net.downwithdestruction.dwdfly;

import net.downwithdestruction.dwdfly.command.CmdFly;
import net.downwithdestruction.dwdfly.command.CmdFlySpeed;
import net.downwithdestruction.dwdfly.configuration.Lang;
import net.downwithdestruction.dwdfly.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        getCommand("fly").setExecutor(new CmdFly(this));
        getCommand("flyspeed").setExecutor(new CmdFlySpeed());

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }
}
