package net.downwithdestruction.dwdfly.command;

import net.downwithdestruction.dwdfly.Logger;
import net.downwithdestruction.dwdfly.Main;
import net.downwithdestruction.dwdfly.configuration.Config;
import net.downwithdestruction.dwdfly.configuration.Lang;
import net.downwithdestruction.dwdfly.manager.ChatManager;
import net.downwithdestruction.dwdfly.task.NoFallDamage;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdFly implements TabExecutor {
    private final Main plugin;

    public CmdFly(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> list = new ArrayList<>();
        if (args.length == 1) {
            String name = args[0];
            list.addAll(Bukkit.getOnlinePlayers().stream()
                    .filter(target -> target.getName().toLowerCase().startsWith(name))
                    .map(Player::getName).collect(Collectors.toList()));
        }
        return list;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.fly")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
                return false; // show usage
            }

            Player player = (Player) sender;
            boolean enableFly = !player.getAllowFlight();

            player.setAllowFlight(enableFly);
            if (!enableFly) {
                player.setFlying(false);
                player.setFlySpeed(0.1F);
                new NoFallDamage(player).runTaskTimer(plugin, 5, 5);
            }

            Logger.debug(player.getName() + " fly mode toggled " + BooleanUtils.toStringOnOff(enableFly));

            ChatManager.sendMessage(sender, Lang.FLY_SET.replace("{action}", BooleanUtils.toStringOnOff(enableFly)));
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            Config.reload();
            Lang.reload(true);
            ChatManager.sendMessage(sender, Lang.RELOAD.replace("{plugin}", plugin.getName()).replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        if (!sender.hasPermission("command.fly.others")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            ChatManager.sendMessage(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }

        if (target.hasPermission("exempt.fly")) {
            ChatManager.sendMessage(sender, Lang.FLY_EXEMPT);
            return true;
        }

        boolean enableFly = !target.getAllowFlight();

        target.setAllowFlight(enableFly);
        if (!enableFly) {
            target.setFlying(false);
            target.setFlySpeed(0.1F);
            new NoFallDamage(target).runTaskTimer(plugin, 5, 5);
        }

        Logger.debug(sender.getName() + " toggled fly mode " + BooleanUtils.toStringOnOff(enableFly) + " for " + target.getName());

        ChatManager.sendMessage(target, Lang.FLY_SET.replace("{action}", BooleanUtils.toStringOnOff(enableFly)));
        ChatManager.sendMessage(sender, Lang.FLY_SET_OTHER.replace("{action}", BooleanUtils.toStringOnOff(enableFly)).replace("{target}", target.getName()));
        return true;
    }
}
