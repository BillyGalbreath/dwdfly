package net.downwithdestruction.dwdfly.command;

import net.downwithdestruction.dwdfly.Logger;
import net.downwithdestruction.dwdfly.configuration.Lang;
import net.downwithdestruction.dwdfly.manager.ChatManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdFlySpeed implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.flyspeed")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            ChatManager.sendMessage(sender, Lang.MUST_SPECIFY_SPEED);
            return false; // show usage
        }

        float flySpeed;
        try {
            flySpeed = (float) Integer.valueOf(args[0]);
        } catch (NumberFormatException e1) {
            try {
                flySpeed = Float.valueOf(args[0]);
            } catch (NumberFormatException e2) {
                ChatManager.sendMessage(sender, Lang.INVALID_NUMBER);
                return false; // show usage
            }
        }

        if (flySpeed < 0 || flySpeed > 10) {
            ChatManager.sendMessage(sender, Lang.INVALID_NUMBER);
            return false; // show usage
        }

        if (args.length == 2) {
            if (!sender.hasPermission("command.flyspeed.others")) {
                ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }

            Player target = Bukkit.getPlayer(args[1]);
            if (target == null) {
                ChatManager.sendMessage(sender, Lang.PLAYER_NOT_FOUND);
                return true;
            }

            if (target.hasPermission("exempt.flyspeed")) {
                ChatManager.sendMessage(sender, Lang.FLYSPEED_EXEMPT);
                return true;
            }

            if (failedLimit(target, flySpeed)) {
                ChatManager.sendMessage(sender, Lang.BEYOND_LIMIT_OTHER.replace("{target}", target.getName()));
                return true;
            }

            Logger.debug(sender.getName() + " set fly speed to " + flySpeed + " for " + target.getName());

            target.setFlySpeed(flySpeed / 10F);

            ChatManager.sendMessage(target, Lang.FLYSPEED_SET.replace("{speed}", Float.toString(flySpeed)));
            ChatManager.sendMessage(sender, Lang.FLYSPEED_SET_OTHER.replace("{speed}", Float.toString(flySpeed)).replace("{target}", target.getName()));
            return true;
        }

        if (!(sender instanceof Player)) {
            ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
            return false; // show usage
        }

        if (failedLimit(sender, flySpeed)) {
            ChatManager.sendMessage(sender, Lang.BEYOND_LIMIT);
            return true;
        }

        Logger.debug(sender.getName() + " set fly speed to " + flySpeed);

        ((Player) sender).setFlySpeed(flySpeed / 10F);

        ChatManager.sendMessage(sender, Lang.FLYSPEED_SET.replace("{speed}", Float.toString(flySpeed)));
        return true;
    }

    private boolean failedLimit(CommandSender target, float limit) {
        if (target.hasPermission("command.flyspeed.limit.*")) {
            return false; // has no limit
        }

        for (int i = (int) Math.floor(limit); i <= 10; i++) {
            if (target.hasPermission("command.flyspeed.limit." + i)) {
                return false; // has limit or higher perm
            }
        }

        return true; // limit is too low or no perms
    }
}
