package net.downwithdestruction.dwdfly.configuration;

import net.downwithdestruction.dwdfly.Logger;
import net.downwithdestruction.dwdfly.Main;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    COMMAND_NO_PERMISSION("&4[&7DwD&4] &4You do not have permission for this command!"),
    PLAYER_COMMAND("&4[&7DwD&4] &4Player only command!"),
    PLAYER_NOT_FOUND("&4[&7DwD&4] &4Player not online!"),
    MUST_SPECIFY_SPEED("&4[&7DwD&4] &4Must specify fly speed!"),
    INVALID_NUMBER("&4[&7DwD&4] &4Invalid number specified!"),
    BEYOND_LIMIT("&4[&7DwD&4] &4That speed is beyond your allowed limit!"),
    BEYOND_LIMIT_OTHER("&4[&7DwD&4] &4That speed is beyond {target}'s allowed limit!"),

    FLY_EXEMPT("&4[&7DwD&4] &4You cannot toggle that player's fly mode!"),
    FLYSPEED_EXEMPT("&4[&7DwD&4] &4You cannot change that player's fly speed!"),

    FLY_SET("&4[&7DwD&4] &6Fly mode toggled {action}"),
    FLY_SET_OTHER("&4[&7DwD&4] &6Fly mode toggled {action} for {target}"),

    FLYSPEED_SET("&4[&7DwD&4] &6Fly speed set to {speed}"),
    FLYSPEED_SET_OTHER("&4[&7DwD&4] &6Fly speed set to {speed} for {target}"),

    RELOAD("&4[&7DwD&4] &6{plugin} v{version} reloaded");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Main.getPlugin(Main.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Main.getPlugin(Main.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
